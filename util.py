#!/usr/bin/env python3
import argparse
import re
from collections import deque

parser = argparse.ArgumentParser(prog="./lpu.py",description="Parse logs and profit.",epilog="If multiple options are used at once, the result is the intersection of their results.")
parser.add_argument("filename", nargs="?", default="-", metavar="FILE", type=argparse.FileType("r"), help="If FILENAME is omitted, standard input is used instead.")
parser.add_argument("-f", "--first", nargs="+", default=False, type=int, metavar="", help="Print first NUM lines")
parser.add_argument("-l", "--last", nargs="+", default=False, type=int, metavar="", help="Print last NUM lines")
parser.add_argument("-t", "--timestamps", action="store_true", help="Print lines that contain a timestamp in HH:MM:SS format")
parser.add_argument("-i", "--ipv4", action="store_true", help="Print lines that contain an IPv4 address, matching IPs are highlighted")
parser.add_argument("-I", "--ipv6", action="store_true", help="Print lines that contain an IPv6 address (standard notation), matching IPs are highlighted")

args = parser.parse_args()
ipv4_regex = r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
ipv6_regex = r"(?!^(?:(?:.*(?:::.*::|:::).*)|::|[0:]+[01]|.*[^:]:|[0-9a-fA-F](?:.*:.*){8}[0-9a-fA-F]|(?:[0-9a-fA-F]:){1,6}[0-9a-fA-F])$)^(?:(::|[0-9a-fA-F]{1,4}:{1,2})([0-9a-fA-F]{1,4}:{1,2}){0,6}([0-9a-fA-F]{1,4}|::)?)$"
time_regex = r"(?:[01]\d|2[0-3]):(?:[0-5]\d):(?:[0-5]\d)"

if args.first != False:
    firstN = deque(maxlen=args.first[0])
else:
    firstN = deque()

if args.last != False:
    lastN = deque(maxlen=args.last[0])
else:
    lastN = deque()


def read_lines():
    try:
        with args.filename as file:
            for idx, line in enumerate(file):
                sifter(linen=idx, line=line.rstrip())
    except:
        print("cant read file/stdin")

def sifter(linen, line):
    o = {
        "ipv4_io": args.ipv4,
        "ipv4_res": False,
        "ipv6_io": args.ipv6,
        "ipv6_res": False,
        "time_io": args.timestamps,
        "time_res": False,
        "payload": line.split()
    }

    if o["ipv4_io"] == True:
        for idx, word in enumerate(o["payload"]):
            if re.match(ipv4_regex, word):
                o["payload"][idx] = word
                o["ipv4_res"] = True

    if o["ipv6_io"] == True:
        for idx, word in enumerate(o["payload"]):
            if re.match(ipv6_regex, word):
                o["payload"][idx] = word
                o["ipv6_res"] = True
    
    if o["time_io"] == True:
        for idx, word in enumerate(o["payload"]):
            if re.match(time_regex, word):
                o["payload"][idx] = word
                o["time_res"] = True

    if o["payload"] != []:
        if (
            (o["ipv4_io"] == o["ipv4_res"]) == True and
            (o["ipv6_io"] == o["ipv6_res"]) == True and 
            (o["time_io"] == o["time_res"]) == True
        ):
            line_printer(linen=linen, line=" ".join(o["payload"]))

def line_printer(linen, line):
    if args.first == False and args.last == False:
        print(line)
    else:
        stack_printer(line=line, store=True, linen=linen, firstn=firstN, lastn=lastN)

def stack_printer(line, linen, store, firstn, lastn):
    p = "line "+str(linen)+": "+line
    if store == True:
        if args.first != False:
            try:
                firstn.insert(linen, p)
            except:
                pass
        
        if args.last != False:
            try:
                if len(lastn) != args.last[0]:
                    lastn.insert(linen, p)
                else:
                    lastn.popleft()   
                    lastn.insert(linen, p)
            except:
                pass

    if store == False:
        if (args.first != False) and (args.last == False):
            for i, item in enumerate(firstn):
                print(firstn[i])
        if (args.first == False) and (args.last != False):
            for i, item in enumerate(lastn):
                print(lastn[i])

        if (args.first != False) and (args.last != False):
            firstLinesSet = set(firstn)
            lastLinesSet = set(lastn)
            intersection = firstLinesSet.intersection(lastLinesSet)
            for i in intersection:
                print(i)

def main():
    read_lines()
    stack_printer(line="", store=False, linen=0, firstn=firstN, lastn=lastN)

main()
